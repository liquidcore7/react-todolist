import axios from 'axios';


const apiUrl = 'https://todolist-backend-springboot.herokuapp.com/';


const HttpOptions = {
    fetchTodos: () => axios({
        method: "get",
        url: apiUrl
    }),
    postTodo: (payload) => axios({
        method: "post",
        url: apiUrl,
        data: payload
    }),
    putTodo: (id, payload) => axios({
        method: "put",
        url: apiUrl + id.toString(),
        data: payload
    }),
    deleteTodo: (id) => axios({
        method: "delete",
        url: apiUrl + id.toString()
    })
};

export default HttpOptions;
import React from 'react'
import './NavBar.css'
import {Navbar, NavbarBrand} from 'reactstrap';
import AddNewButton from "../add-new-button/AddNewButton";


function NavBar(props) {
    return (
        <Navbar className="NavBar">
            <NavbarBrand className="icon">
                Punchello
            </NavbarBrand>
            <AddNewButton refetchTodos={props.refetchTodos}/>
        </Navbar>
    );
}

export default NavBar;
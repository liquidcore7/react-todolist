import React from 'react'
import TodoItem from "../todo-item/TodoItem";
import ErrorBanner from "../error-banner/ErrorBanner";
import './TodoList.css'
import {Container, Row, Col} from "reactstrap";
import LoadingSpinner from "../loading-spinner/LoadingSpinner";


class TodoList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            todoItems: [],
            loaded: false,
            error: null
        };
    }

    async componentDidMount() {
        await this.props.refetchTodos(this);
    }

    render() {
        const todoCards = this.state.todoItems.map(todoItem =>
            <Col key={todoItem.id} xl='3' lg='4' md='6' sm='12' className="my-3">
                <TodoItem id={todoItem.id}
                          title={todoItem.title}
                          text={todoItem.text}
                          refetchTodos={async () => await this.props.refetchTodos(this)}/>
            </Col>
        );

        return (
            <>
                {this.state.error && <ErrorBanner error={this.state.error.toString()}/>}
                <Container fluid={true}>
                    {this.state.loaded
                        ? <Row>{todoCards}</Row>
                        : <Row className="justify-content-center"><LoadingSpinner/></Row>}
                </Container>
            </>
        );
    }

}

export default TodoList;
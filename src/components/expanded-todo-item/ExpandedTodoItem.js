import React from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter, Button, Input} from 'reactstrap'
import '../todo-item/TodoItem.css'
import './ExpandedTodoItem.css'


class ExpandedTodoItem extends React.Component {

    constructor(props) {
        super(props);
        this.titleInput = null;
        this.textInput = null;
    }

    render() {
        return (
            <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} className="TodoItem" size="xl">
                <ModalHeader toggle={this.props.toggle} className="todo-title">
                    <Input placeholder="Short task title"
                           innerRef={(ref) => {
                               this.titleInput = ref;
                               if (ref) ref.value = this.props.title;
                           }
                    } />
                </ModalHeader>
                <ModalBody className="todo-text">
                    <Input placeholder="Detailed task description"
                           className="todo-empty-textarea"
                           type="textarea"
                           innerRef={(ref) => {
                               this.textInput = ref;
                               if (ref) ref.value = this.props.text;
                           }
                    } />
                </ModalBody>
                <ModalFooter>
                    <Button color="primary"
                            onClick={() => this.props.submitButton.handler(this.titleInput.value, this.textInput.value)}>
                                {this.props.submitButton.name}
                    </Button>
                    <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

export default ExpandedTodoItem;


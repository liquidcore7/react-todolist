import React from 'react'
import {Button} from 'reactstrap'
import ExpandedTodoItem from "../expanded-todo-item/ExpandedTodoItem";
import HttpOptions from "../../config";
import ErrorBanner from "../error-banner/ErrorBanner";
import './AddNewButton.css'


class AddNewButton extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dialogOpen: false,
            error: null
        };
    }

    toggleDialog = () => this.setState((prev, props) => ({
        dialogOpen: !prev.dialogOpen
    }));

    postTodo = async (title, text) => {
        await HttpOptions.postTodo({title: title, text: text})
            .then(response => this.setState({dialogOpen: false}))
            .then(this.props.refetchTodos)
            .catch(err => this.setState({
                dialogOpen: false,
                error: err
            }));
    };

    render() {
        return (
            <>
                {this.state.error ? <ErrorBanner error={this.state.error}/> : null}
                <Button className="AddNewButton btn-outline-light shadow-none"
                        color="transparent"
                        size="lg"
                        onClick={this.toggleDialog}>
                    Add item
                </Button>
                <ExpandedTodoItem isOpen={this.state.dialogOpen}
                                  toggle={this.toggleDialog}
                                  text={""}
                                  title={""}
                                  submitButton={{name: "Add TODO", handler: this.postTodo}}
                />
            </>
        );
    };
}

export default AddNewButton;
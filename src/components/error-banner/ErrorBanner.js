import React from 'react'
import {Toast, ToastHeader, ToastBody} from 'reactstrap'
import './ErrorBanner.css'


function ErrorBanner(props) {
    return (
        <Toast className="ErrorBanner rounded bg-danger">
            <ToastHeader className="err-header justify-content-center">Error!</ToastHeader>
            <ToastBody>{props.error}</ToastBody>
        </Toast>
    );
}

export default ErrorBanner;
import React from 'react'
import './TodoItem.css'
import {Card, CardHeader, CardBody, CardText, CardFooter, Button, Row, Col, Container} from 'reactstrap'
import ExpandedTodoItem from "../expanded-todo-item/ExpandedTodoItem";
import HttpOptions from "../../config";



class TodoItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            expanded: false
        }
    }


    trimText = (text) => text.length > 140 ? text.substr(0, 137) + "..." : text;

    toggleExpanded = () => {
        this.setState((prev, props) => ({
            expanded: !prev.expanded
        }));
    };

    updateTodo = async (title, text) => {
        HttpOptions
            .putTodo(this.props.id,{title: title, text: text})
            .then(this.props.refetchTodos);
    };

    deleteTodo = async () => {
        HttpOptions
            .deleteTodo(this.props.id)
            .then(this.props.refetchTodos);
    };

    render() {
        return (
            <>
                <Card className="TodoItem h-100 mx-2">
                    <CardHeader>
                        <Container fluid={true}>
                            <Row className="align-items-center">
                                <Col xs='9' className='align-items-center'>
                                    <div className="todo-title">{this.props.title}</div>
                                </Col>
                                <Col xs='3' className='align-items-center'>
                                    <Button className="btn-sm btn-outline-danger shadow-none todo-cancel-button"
                                            color='transparent'
                                            onClick={this.deleteTodo}>Remove</Button>
                                </Col>
                            </Row>
                        </Container>
                    </CardHeader>
                    <CardBody>
                        <CardText className="todo-text">{this.trimText(this.props.text)}</CardText>
                    </CardBody>
                    <CardFooter>
                        <Button className="todo-button" onClick={this.toggleExpanded}>Open TODO</Button>
                    </CardFooter>
                </Card>

                <ExpandedTodoItem isOpen={this.state.expanded}
                                  toggle={this.toggleExpanded}
                                  title={this.props.title}
                                  text={this.props.text}
                                  submitButton={{name: "Update TODO", handler: this.updateTodo}}
                />
            </>
        );
    }
}

export default TodoItem;
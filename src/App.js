import React from 'react';
import TodoList from "./components/todo-list/TodoList";
import NavBar from "./components/navbar/NavBar";
import HttpOptions from "./config";



class App extends React.Component {

    constructor(props) {
        super(props);
    };

    refetchTodos = async component => {
        component.setState({
            todoItems: [],
            loaded: false
        });

        return await HttpOptions
            .fetchTodos()
            .then(response => component.setState({
                todoItems: response.data || [],
                error: null,
                loaded: true
            }))
            .catch(err => this.setState({
                todoItems: [],
                loaded: true,
                error: err
            }));
    };

    render() {
        const todoList = <TodoList ref="todolist" refetchTodos={this.refetchTodos}/>;
        return (
            <>
                <NavBar refetchTodos={async () => await this.refetchTodos(this.refs.todolist)}/>
                {todoList}
            </>
        );
    }


}

export default App;
